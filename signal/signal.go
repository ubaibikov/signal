package signal
/*
	пакет сигнал служет для создани / прослушки событий
*/

import (
	"fmt"
	"math/rand"
	"sync"
)

// FuncErr принимаемый тип возращающий ошибку
type FuncErr func() error

// Signaler интерефейс предоставляющий методы работу наружу
type Signaler interface {
	// Listen прослушка события
	Listen(f FuncErr)

	// Send создания события
	Send()

	// Close закрытие события
	Close()
}

type signal struct {
	sync.Mutex

	group, groupSend, groupCheck map[int]struct{}
	state, close bool
}

func New() Signaler {
	return &signal{
		group: 		make(map[int]struct{}),
		groupSend: 	make(map[int]struct{}),
		groupCheck: make(map[int]struct{}),
	}
}

func (s *signal) read() bool {
	s.Lock()
	defer s.Unlock()

	if s.close {
		return false
	}

	// проверяем состояние сигнала
	// размеры группы с группой отправиленных собщений потокам
	if s.state && len(s.group) != len(s.groupSend) {
		for i := range s.group {
			if _, ok := s.groupSend[i]; !ok {
				fmt.Printf("отправили [%d] id-шнику \n", i)
				s.groupSend[i] = struct{}{}
			}
		}
	}

	return true
}

// add добаялет поток в общую массу
func (s *signal) add(id int) {
	s.Lock()
	defer s.Unlock()
	s.group[id] = struct{}{}
}

// check проверят поток на наличие сигнала отправки
func (s *signal) check(id int) bool {
	s.Lock()
	defer s.Unlock()

	if _, ok := s.groupSend[id]; ok {
		if _, ok := s.groupCheck[id]; !ok {
			s.groupCheck[id] = struct{}{}
			return true
		}
	}

	return false
}

// Listen слушает сигнал и выполняет действие , если что-то произошло
func (s *signal) Listen(f FuncErr) {
	s.listen(f)
}

// listen обертка для Listen со всем "сложным" функционалом
func (s *signal) listen(f FuncErr) {
	id := rand.Int()
	s.add(id)
	for s.read() {
		if s.check(id) {
			if err := f(); err != nil {
				fmt.Println(fmt.Errorf("ошибка произошедшия в итоге функционала f() err : [%v]", err))
			}
		}
	}
}

// Send создание действия
func (s *signal) Send() {
	stateEdit(s, &s.state, true)
}

// Close закрытия и или завершения действия
func (s *signal) Close() {
	stateEdit(s, &s.close, true)
}

// stateEdit  всопомгательная функция для измения состония
func stateEdit(m sync.Locker, v *bool, state bool) {
	m.Lock()
	defer m.Unlock()
	*v = state
}