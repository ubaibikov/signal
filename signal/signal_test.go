package signal

import (
	"github.com/stretchr/testify/require"
	"sync"
	"testing"
	"time"
)

var (
	wg sync.WaitGroup
	m  sync.Mutex
)


func Test_Signal(t *testing.T) {
	var (
		s 	 = New()
		want = 3
		actual int
	)

	wg.Add(3)

	go func() {
		defer wg.Done()
		s.Listen(func() error {
			inc(&actual)
			return nil
		})
	}()

	go func() {
		defer wg.Done()
		s.Listen(func() error {
			inc(&actual)
			return nil
		})
	}()

	go func() {
		defer wg.Done()
		s.Listen(func() error {
			inc(&actual)
			return nil
		})
	}()

	s.Send()

	time.Sleep(time.Second * 1)
	s.Close()
	wg.Wait()

	require.Equal(t, want, actual)
}

func BenchmarkSignal(t *testing.B) {
	var s = New()

	wg.Add(10000)
	for i := 0; i < 10000; i++ {
		go func() {
			defer wg.Done()
			s.Listen(func() error {
				return nil
			})
		}()
	}
	s.Send()

	s.Close()
	wg.Wait()
}

func inc(i *int) {
	m.Lock()
	*i++
	m.Unlock()
}